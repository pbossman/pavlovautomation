const Netcat = require('node-netcat');
const TailFile = require('@logdna/tail-file')
const split2 = require('split2') // A common and efficient line splitter
const path = require('path');
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
let env = process.env.NODE_ENV || 'vultr';
let config = require('./config/config.js')[env];
let userMods = require('./config/userMods.js');
let matchMods = require('./config/matchMods.js');
//console.log(config);
//console.log(userMods);
//console.log(matchMods);

let rconConnected = false;
const client = new Netcat.client(config.rcon.rconport, config.rcon.hostip, {timeout: config.rcon.timeout} );

const tail = new TailFile('/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log')
let username = '';
let userid = '';
let mapType = '';
let mapName = '';
let index = 0;
let setSkin = false;
userIndex = 0;
class Match {
	constructor(mapName, mapFilename, mapType, mapMode, gameState, mod, chaos, blueSkinType, redSkinType) {
		this.mapName = mapName;
		this.mapFilename = mapFilename;
		this.mapType = mapType;
		this.mapMode = mapMode;
		this.gameState = gameState;
	    this.mod = mod;
		this.chaos = chaos;
		this.blueSkinType = blueSkinType;
		this.redSkinType = redSkinType;
		this.timeoutObj = {};
	}
}

const currentMatch = new Match('init','init','init','init','init',false,false,'','');
//console.log(currentMatch);

class User {
	constructor(username, userid) {
        	this.username = username;
        	this.userid = userid;
		this.skinType = "";
        	this.stOverrideMap = false;
		this.gunType = "";
		this.items = [];
		this.tk = 0;
    }
}

const matchPlayers = [];

client.on('close', function(){
    rconConnected = false;
    console.log('RCON connetion closed');
    client.start();
});

client.on('data', rcRetur => {
    onData(rcRetur);
});

function onData(rcReturn){
    if(rcReturn.includes("true")){
        return "success";
    } else if (rcReturn.includes("false")){
        return "an error occurred " + rcReturn
    } else if (rcReturn.includes("Password")){
        client.send(config.rcon.password);
        return;
    } else if (rcReturn.includes("Authenticated")) {
        if(rcReturn.includes("0")){
            console.log("Invalid Password");
            return;
        }
        rconConnected = true;
        console.log("RCON connected");
        return;
    }
    let RCData = rcReturn+"";
    RCData = RCData.trim();
    if (RCData.length > 0){
        RCData = parse(rcReturn+"");
        console.log('parsed RCData :', RCData);
    }
    if (RCData == "ERROR"){
        return;
    }
    if (RCData.ServerInfo!=null){
        // handle serverinfo
        return;
    }
    if (RCData.PlayerList!=null){
        if(RCData.PlayerList.length==0){
            return "no players on server"
        }
        //handle player list
    }
    if (setSkin && RCData.PlayerInfo!=null){
        let skinType = '';
        // user skin overrides map
        if (matchPlayers[userIndex].username == RCData.PlayerInfo.PlayerName &&
            matchPlayers[userIndex].stOverrideMap && 
            matchPlayers[userIndex].skinType.length > 0){
            skinType = matchPlayers[userIndex].skinType;
        } else if(RCData.PlayerInfo.TeamId == 0){
            skinType = currentMatch.blueSkinType;
        } else {
            skinType = currentMatch.redSkinType
        }
        cmdText = "SetPlayerSkin " + RCData.PlayerInfo.PlayerName + ' ' + skinType;
        setSkin = false;
        userIndex = 0;
        console.log(cmdText);
        respData = rconSend(cmdText);
    }
    return RCData;
}

client.start();

tail
  .on('tail_error', (err) => {
      console.error('TailFile had an error!', err)
      throw err
    })
  .start()
  .catch((err) => {
      console.error('Cannot start.  Does the file exist?', err)
      throw err
    })

// Data won't start flowing until piping

tail
  .pipe(split2())
  .on('data', async (line) => 
  {
      logLine = line.split("]")[2];
      // find triggering log events
      // user joined
      if (logLine.substring(0,23) == "LogNet: Join succeeded:") {
          username = logLine.split("LogNet: Join succeeded:")[1];
          console.log("!logMon user joined: " + username);
          console.log('Map : ' + currentMatch.mapName + ' Mode: ' + currentMatch.mapMode)

	  // check if match has a skin preference
	  
	  if (currentMatch.mod) {
             setSkin = true;
          }
            // if user has set skin
	  for (index = 0; index < matchPlayers.length; ++index) {
		  if (username == matchPlayers[index].username &&
	              matchPlayers[index].skinType.length > 0) {
			  setSkin = true;
                          userIndex = index;
		  }
	  }
	  if (setSkin) {
		  cmdText = "InspectPlayer " + username;
		  const respData = rconSend(cmdText);
	  }
	  return;
       
      // loading new map and mode
      } else if (logLine.substring(0,17) == "LogLoad: LoadMap:") {
	      console.log ("!logMon Loading map")
	      currentMatch.mapType = logLine.split(`/`)[2];
	      if (currentMatch.mapType == 'CustomMaps'){
	         currentMatch.mapName = logLine.substring(logLine.lastIndexOf("/") + 1, logLine.length);
	         currentMatch.mapName = currentMatch.mapName.split('??listen?')[0];
	      } else {
                 currentMatch.mapName = logLine.split('/')[4];
                 currentMatch.mapName = currentMatch.mapName.split('??listen?')[0];
	      }
	      currentMatch.mapFilename = logLine.split(`/`)[3];
	      currentMatch.mapMode = logLine.split(`game=`)[1];
	      currentMatch.gameState = 'Starting'
          console.log('current match data - before override search: ', currentMatch)
          console.log('matchMods ', matchMods)
          // search and set map overrides
          currentMatch.mod = false;
	      currentMatch.chaos = false;
	      currentMatch.blueSkinType = "";
	      currentMatch.redSkinType = "";
	      currentMatch.timeoutObj = {};
	      for (index = 0; index < matchMods.map.length; index++) {
		      if (currentMatch.mapName == matchMods.map[index].name 
		       && currentMatch.mapMode == matchMods.map[index].mode) {
			      currentMatch.mod = true;
			      currentMatch.chaos = matchMods.map[index].chaos;
			      currentMatch.blueSkinType = matchMods.map[index].blueSkinType;
			      currentMatch.redSkinType  = matchMods.map[index].redSkinType; 
			      console.log('map loading has overrides');
			      if (currentMatch.chaos) {
				      currentMatch.timeoutObj = setTimeout(chaosSkin, 600000);
			      }
		      }
	      }
	      console.log ('current Match after override search: ', currentMatch);
      // new map loaded
      } else if (logLine.substring(0,24) == "PavlovLog: LoadComplete:"){
              console.log("!logMon Loaded map")
	      mapType = logLine.split(`/`)[2];
	      mapFile = logLine.split(`/`)[3];
	      mapName = logLine.split(`/`)[4];
	      if (mapType == currentMatch.mapType && mapName == currentMatch.mapName) {
		      currentMatch.gameState = "Started";
	      }
	      //console.log (currentMatch)
      // game ended reset match and player data
      } else if ((logLine.substring(0,77) ==   "LogGameMode: Display: Match State Changed from InProgress to WaitingPostMatch")
               || (logLine.substring(0,71) == "LogGameMode: Display: Match State Changed from InProgress to LeavingMap")) {
	      console.log("!logMon entering match ended ");
	      currentMatch.gameState = 'Ended';
	      console.log(currentMatch);
	      matchPlayers.splice(0,index);
          currentMatch.name = "";
          currentMatch.mode = "";
	      currentMatch.mod = false;
	      currentMatch.chaos = false;
	      currentMatch.blueSkinType = "";
	      currentMatch.redSkinType = "";
	      currentMatch.timeoutObj = {};
	      console.log('total players ' + matchPlayers.length);
      } else if (logLine.substring(0,22) == "LogNet: Login request:") {
	      console.log("!logMon entering login request ");
	      userid = logLine.split("userId:")[1]; 
	      userid = userid.split("NULL:")[1]
	      userid = userid.split("platform:")[0];
	      userid = userid.trim();
	      username = logLine.split("Name=")[1];
	      username = username.split("?playerHeight=")[0];
	      username = username.trim();
	      let currentUser = new User(username, userid);
	      for (index = 0; index < userMods.users.length; ++index) {
		      if (username == userMods.users[index].username){
			      currentUser.skinType = userMods.users[index].skinType
			      currentUser.stOverrideMap = userMods.users[index].stOverrideMap
			      currentUser.gunType = userMods.users[index].gunType
			      currentUser.items = userMods.users[index].items;
			      if (currentUser.skinType.length > 0){
				      setSkin = true;
			      }
		      }
	      }
	      matchPlayers.push(currentUser);
	      console.log('match player count: ' + matchPlayers.length)
              console.log(matchPlayers);
              if (currentMatch.mod) {
		      setSkin = true;
	      }
              if (setSkin) {
                      cmdText = "InspectPlayer " + username;
                      const respData = rconSend(cmdText);
              }
	      //matchPlayers.push(currentUser);
	      //console.log('match player count: ' + matchPlayers.length)
	      //console.log(matchPlayers);
      } else if (logLine.substring(0,26) == "LogNet: UChannel::CleanUp:"
	      || logLine.substring(0,24) == "LogNet: UChannel::Close:")  {
	      console.log("!logMon user departing ")
	      userid = line.split("UniqueId: NULL:")[1];
	      userid = userid.trim();
	      console.log("!logMon userId " + userid + " left.");
	      for (index = 0; index < matchPlayers.length; ++index) {
		      if (matchPlayers[index].userid == userid) {
			      console.log("ilogMon departing player: " + matchPlayers[index].username + ' ' + matchPlayers[index].userid );
			      matchPlayers.splice(index,1);
		      }
	      }
	      console.log('remaining player count: ' + matchPlayers.length);
      } else if (logLine.substring(0,38) == 'PavlovLog: Warning: Shot failed sanity'){
	      console.log(logline);
	      userid = logline.split("for player")[1];
              userid = userid.trim();
              warnIdx = logline.split("(")[1];
              console.log("warn TK user " + userid); 
              console.log("warn index " + warnIdx);
              for (index = 0; index < matchPlayers.length; ++index) {
		      if (matchPlayers[index].userid == userid) {
                              matchPlayers[index].tk = matchPlayers[index].tk + 1;
                      }
              }
      }
	     // PavlovLog: Warning: Shot failed sanity checks (01) for player zTriplerz
    })

// function to switch ~half players of each side skin, meant to be executed mid-match.
function chaosSkin(){
	let blueIdx = 0;
	let redIdx = 0;
	for (index = 0; index < matchPlayers.length; ++index) {
		if (matchPlayers[index].teamId == 0) {
			matchPlayers[index].teamIdx = blueIdx;
			blueIdx = blueIdx + 1;
			if (blueIdx % 2 == 1){
				skinType = currentMatch.redSkinType;
				matchPlayers[index].skinType = skinType;
			}
		} else {
			matchPlayers[index].teamIdx = redIdx;
                        redIdx = redIdx + 1;
			if (redIdx % 2 == 1) {
				skinType = currentMatch.blueSkinType;
				matchPlayers[index].username = skinType;
                      }
                }
	        cmdText = "SetPlayerSkin " + matchPlayers[index].username + ' ' + skinType;
                console.log(cmdText);
                const respData = rconSend(cmdText);
		return;
	}
}

function rconSend(st){
        if (!rconConnected){
		console.log('connect to rcon')
		console.log('send password ' + config.rcon.password);
                client.send(config.rcon.password);
		console.log('send command ' + st);
                client.send(st)
        } else {
                client.send(st);
        }
}

function parse(rawJSON){
        try{
                return JSON.parse(rawJSON);
        }catch(err){
                console.log("Caught -->"+err+"<--     "+rawJSON);
                return "ERROR";
        }
}
