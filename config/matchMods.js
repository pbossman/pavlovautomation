let matchMods = {
	map: [ 
		{
			name:       "Santorini",
			mode:          "TDM",
			blueSkinType:  "us",
			redSkinType:   "german",
			chaos:         true
		},
		                {
                        name:       "Santorini",
                        mode:       "DM",
                        blueSkinType:  "german",
                        chaos:         false
                },
		{
                        name:       "bridge",
                        mode:       "TDM",
                        blueSkinType:  "naked",
                        redSkinType:   "farmer",
                        chaos:         true
                }
	]
}
module.exports = matchMods;
