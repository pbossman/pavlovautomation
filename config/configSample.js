var config = {
    arctic: {
        //Rcon connection settings
        rcon: {
            hostip:   "51.222.10.73",
            rconport:     15252,
            nodeport:     8080,
            password: "md5 hashed pw here",
            timeout:  500,
            defaultUniqueId: "zTriplerz",
            alpha: "zTriplerz",
            beta: "zTriplerz",
            charlie: "Skylar_Cal-141"
        }
    },
    pavlovhorde: {
        //Rcon connection settings
        rcon: {
            hostip:   "140.82.24.73",
            rconport:     9100,
            nodeport:     8080,
            password: "md5 hashed pw here",
            timeout:  500,
            defaultUniqueId: "zTriplerz",
            alpha: "zTriplerz",
            beta: "zTriplerz",
            charlie: "Skylar_Cal-141"
	}
    },
    vultr: {
        //Rcon connection settings
        rcon: {
            hostip:   "localhost",
            rconport:        9100,
            nodeport:        8080,
            password: "md6 hashed pw here",
            timeout:  2147483647
        }
    }
};
module.exports = config;
