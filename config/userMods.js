let userMods = {
	users: [ 
		{
			username:   "Skylar_Cal-141",
			skinType:   "soviet",
			stOverrideMap: true,
			gunType:    "",
			items:      []
		},
		                {
                        username:   "zTriplerz",
                        skinType:   "farmer",
		        stOverrideMap: false,
                        gunType:    "stg44",
                        items:      ["grenade", "flash"]
                }
	]
}
module.exports = userMods;
